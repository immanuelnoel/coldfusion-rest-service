<cfcomponent rest="true" restpath="/lend">
	
	<cffunction name="getLends" access="remote" httpmethod="GET" returntype="String" produces="application/json">	
		<cfargument name="lendID" required="false" restargsource="Query" type="numeric"/>		
			
		<!--- Crux of the API --->
		<cfscript>
			
			data = new component("data");
			
			if(structKeyExists(URL, "lendID")){
				for(lend in data.lend){
					if(lend['lendID'] == URL.lendID){
						return serializeJSON(lend, "struct");
					}
				}
				
			} else {
				return serializeJSON(data.lend, "struct");
			}
			
		</cfscript>
		
		<!--- END Crux of the API --->
        
    </cffunction>
    
    <cffunction name="lend" access="remote" httpmethod="POST" returntype="String" produces="application/json">	
		<cfargument name="customerID" required="true" restargsource="Query" type="numeric"/>	
		<cfargument name="BookID" required="true" restargsource="Query" type="numeric"/>			
			
		<!--- Crux of the API --->
		<cfreturn serializeJSON({"success":true}, "struct")>
		<!--- END Crux of the API --->
        
    </cffunction>
	
</cfcomponent>