<cfcomponent rest="true" restpath="/customer">
	
	<cffunction name="getCustomers" access="remote" httpmethod="GET" returntype="String" produces="application/json">	
		<cfargument name="customerID" required="false" restargsource="Query" type="numeric"/>		
			
		<!--- Crux of the API --->
		
		<cfscript>
			
			data = new component("data");
			
			if(structKeyExists(URL, "customerID")){
				for(customer in data.customers){
					if(customer['customerID'] == URL.customerID){
						return serializeJSON(customer, "struct");
					}
				}
				
			} else {
				return serializeJSON(data.customers, "struct");
			}
			
		</cfscript>
		
		<!--- END Crux of the API --->
        
    </cffunction>
	
</cfcomponent>