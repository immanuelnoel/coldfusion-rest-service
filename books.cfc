<cfcomponent rest="true" restpath="/book">
	
	<cffunction name="getBooks" access="remote" httpmethod="GET" returntype="String" produces="application/json">	
		<cfargument name="bookID" required="false" restargsource="Query" type="numeric"/>		
			
		<!--- Crux of the API --->
		
		<cfscript>
						
			data = new component("data");
			
			if(structKeyExists(URL, "bookID")){
				for(book in data.books){
					if(book['bookID'] == URL.bookID){
						return serializeJSON(book, "struct");
					}
				}
				
			} else {
				return serializeJSON(data.books, "struct");
			}
			
		</cfscript>
		
		<!--- END Crux of the API --->
        
    </cffunction>
	
</cfcomponent>