component{
	
	this.books = [];
		
	bookone["bookID"] = 1;
	bookone["bookName"] = "Little Fires Everywhere";
	bookone["author"] = "Celeste Ng";
	bookone["rating"] = 4.17;
	bookone["preview"] = "Everyone in Shaker Heights was talking about it that summer: how Isabelle, the last of the Richardson children, had finally gone around the bend and burned the house down.";
	this.books.append(bookone);
	
	booktwo["bookID"] = 2;
	booktwo["bookName"] = "Into the Water";
	booktwo["author"] = "Paula Hawkins";
	booktwo["rating"] = 3.53;
	booktwo["preview"] = "In the last days before her death, Nel called her sister. Jules didn’t pick up the phone, ignoring her plea for help.";
	this.books.append(booktwo);
	
	this.customers = [];
		
	customerOne["customerID"] = 1;
	customerOne["customerName"] = "John";
	this.customers.append(customerOne);
	
	customerTwo["customerID"] = 2;
	customerTwo["customerName"] = "Jane";
	this.customers.append(customerTwo);
	
	this.lend = [];
		
	lendOne["lendID"] = 1;
	lendOne["bookID"] = 1;
	lendOne["customerID"] = 2;
	this.lend.append(lendOne);
	
	lendTwo["lendID"] = 2;
	lendTwo["bookID"] = 2;
	lendTwo["customerID"] = 1;
	this.lend.append(lendTwo);
}